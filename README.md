# Job Application

This demo project is used to show the skills of an applicant for forestsoft

## Goal to archive
Please implement an Shopware 5 plugin which greets the logged in customer with its name of every 
single page under the logo of the demo shop.

The logo should only displayed on the Desktop version of the shop.

Create your plugin in ./custom/plugins/#YourPluginName#/


For further information read the shopware documentation about creating an plugin:
https://developers.shopware.com/plugin-guide/

## Required software
- Any docker version up to 1.17
- docker-compose
- Your IDE
## Getting started

Clone the repository and run the Container image with 
```bash
you@host$: docker-compose up 
```
Head over to https://localhost and skip the certificate warning. 

To enter the shopware backend type https://localhost in your browser and use the default credentials:

Username: demo
Password: demo


